using System;
using System.Collections.Generic;
using System.Linq;
using FsCheck;
using FsCheck.Xunit;

namespace TddDiamondKata.Test.Property
{
    public class DiamondTest
    {
        [Property(Arbitrary = new[]{typeof(CapitalLetterGenerator)})]
        public FsCheck.Property NotEmpty(char c)
        {
            return Diamond.GenerateRecursively(c).All(s => s != string.Empty).ToProperty();
        }
        
        [Property(Arbitrary = new[]{typeof(CapitalLetterGenerator)})]
        public FsCheck.Property FirstLineContainsA(char c)
        {
            return Diamond.GenerateRecursively(c).First().Contains('A').ToProperty();
        }
        
        [Property(Arbitrary = new[]{typeof(CapitalLetterGenerator)})]
        public FsCheck.Property LastLineContainsA(char c)
        {
            return Diamond.GenerateRecursively(c).Last().Contains('A').ToProperty();
        }
        
        [Property(Arbitrary = new[]{typeof(CapitalLetterGenerator)})]
        public FsCheck.Property DiamondWidthEqualsHeight(char c)
        {
            var diamondLines = Diamond.GenerateRecursively(c).ToList();
            return diamondLines.All(row => row.Length == diamondLines.Count).ToProperty();
        }

        [Property(Arbitrary = new[] {typeof(CapitalLetterGenerator)})]
        public FsCheck.Property SpacesPerRowAreSymmetric(char c)
        {
            var diamondLines = Diamond.GenerateRecursively(c).ToList();
            return diamondLines.All(row => CountLeadingSpaces(row) == CountTrailingSpaces(row)).ToProperty();
        }
        
        [Property(Arbitrary = new[] { typeof(CapitalLetterGenerator) })]
        public FsCheck.Property SymmetricAroundVerticalAxis(char c)
        {
            return Diamond.GenerateRecursively(c).ToList()
                .All(line =>
                {
                    var half = line.Length / 2;
                    var firstHalf = line.Take(half);
                    var secondHalf = line.Skip(half + 1);
                    return firstHalf.Reverse().SequenceEqual(secondHalf);
                }).ToProperty();
        }
        
        [Property(Arbitrary = new[] { typeof(CapitalLetterGenerator) })]
        public FsCheck.Property SymmetricAroundHorizontalAxis(char c)
        {
            var diamond = Diamond.GenerateRecursively(c).ToList();
            var half = diamond.Count / 2;
            var topHalf = diamond.Take(half);
            var bottomHalf = diamond.Skip(half + 1);
            return topHalf.Reverse().SequenceEqual(bottomHalf).ToProperty();
        }
        
        [Property(Arbitrary = new[] {typeof(CapitalLetterGenerator)})]
        public FsCheck.Property InputLetterRowContainsNoOutsidePaddingSpaces(char c)
        {
            var inputLetterRow = Diamond.GenerateRecursively(c).ToList()
                .First(x => x.Contains(c));
            return (!IsSpaceChar(inputLetterRow.First()) && !IsSpaceChar(inputLetterRow.Last())).ToProperty();
        }
        
        [Property(Arbitrary = new[] { typeof(CapitalLetterGenerator) })]
        public FsCheck.Property RowsContainCorrectLetterInCorrectOrder(char c)
        {
            var expected = new List<char>();
            for (var i = 'A'; i < c; i++) expected.Add(i);

            for (var i = c; i >= 'A'; i--) expected.Add(i);

            var actual = Diamond.GenerateRecursively(c).ToList().Select(line => line.First(letter => !IsSpaceChar(letter)));
            return actual.SequenceEqual(expected).ToProperty().Classify(c == 'Z', "Se probó el extremo 'Z'");
        }

        [Property]
        public FsCheck.Property NotCapitalLettersShouldFail(char c)
        {
            return Prop.Throws<ArgumentOutOfRangeException, IEnumerable<string>>(
                new Lazy<IEnumerable<string>>(() => Diamond.GenerateRecursively(c))).When(!CapitalLetterGenerator.IsCapitalLetter(c));
        }
        
        private static int CountTrailingSpaces(string line) =>
            line.Substring(line.LastIndexOf(GetLastNotSpaceChar(line))).Count(IsSpaceChar);

        private static bool IsSpaceChar(char c) => c == ' ';

        private static char GetLastNotSpaceChar(string line) => line.Last(x => !IsSpaceChar(x));
        private static char GetFirstNotSpaceChar(string line) => line.First(x => !IsSpaceChar(x));
        

        private static int CountLeadingSpaces(string line) =>
            line.Substring(0, line.IndexOf(GetFirstNotSpaceChar(line))).Count(IsSpaceChar);
        

        private static class CapitalLetterGenerator
        {
            public static Arbitrary<char> Generate() =>
                Arb.Default.Char().Filter(IsCapitalLetter);

            public static bool IsCapitalLetter(char c) => c is >= 'A' and <= 'Z';
        }
    }
}
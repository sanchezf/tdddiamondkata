using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace TddDiamondKata.Test.Unit
{
    public class DiamondTest
    {
        [Fact]
        public void Generate_WhenA_ShouldReturnA()
        {
            var result = Diamond.Generate('A').ToList();
            result.Should().HaveCount(1);
            result.First().Should().Be("A");
        }
        
        [Theory]
        [InlineData('a')]
        [InlineData('0')]
        [InlineData(',')]
        public void Generate_WhenReceiveNotCapitalLettersChars_ShouldFail(char input)
        {
            var exception = Assert.Throws<ArgumentOutOfRangeException>(() => Diamond.Generate(input).ToList());
            exception.Message.Should()
                .StartWith($"El caracter '{input}' es inválido. Sólo se admiten letras mayúsculas.");
        }
        
        [Fact]
        public void Generate_WhenB_ShouldReturnThreeLines()
        {
            var result = Diamond.Generate('B').ToList();
            result.Should().HaveCount(3);
            result[0].Should().Be(" A ");
            result[1].Should().Be("B B");
            result[2].Should().Be(" A ");
        }
        
        [Fact]
        public void Generate_WhenC_ShouldReturnFiveLines()
        {
            var result = Diamond.Generate('C').ToList();
            result.Should().HaveCount(5);
            result[0].Should().Be("  A  ");
            result[1].Should().Be(" B B ");
            result[2].Should().Be("C   C");
            result[3].Should().Be(" B B ");
            result[4].Should().Be("  A  ");
        }
        
    }    
}
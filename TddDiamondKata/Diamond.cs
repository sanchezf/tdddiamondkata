using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TddDiamondKata
{
    public static class Diamond
    {
        private const char InitialChar = 'A';
        private const char Padding = ' ';

        /// <summary>
        /// Given a letter, print a diamond starting with A , and having the supplied letter at the widest point.
        /// </summary>
        /// <param name="characterAtTheWidestPoint">The character that should be at the widest point</param>
        /// <returns>A collection of lines that forms a diamond</returns>
        /// <exception cref="ArgumentOutOfRangeException">When the argument is not a capital letter</exception>
        public static IEnumerable<string> Generate(char characterAtTheWidestPoint)
        {
            AssertIsCapitalLetter(characterAtTheWidestPoint);
            return GenerateDiamondIterative(characterAtTheWidestPoint);
        }

        /// <summary>
        /// Given a letter, print a diamond starting with A , and having the supplied letter at the widest point.
        /// </summary>
        /// <param name="characterAtTheWidestPoint">The character that should be at the widest point</param>
        /// <returns>A collection of lines that forms a diamond</returns>
        /// <exception cref="ArgumentOutOfRangeException">When the argument is not a capital letter</exception>
        public static IEnumerable<string> GenerateRecursively(char characterAtTheWidestPoint)
        {
            AssertIsCapitalLetter(characterAtTheWidestPoint);
            var lineLength = (characterAtTheWidestPoint - InitialChar) * 2 + 1;
            return GenerateDiamondRecursively(InitialChar, characterAtTheWidestPoint, lineLength);
        }

        // ReSharper disable once UnusedMember.Local
        // ReSharper disable once UnusedParameter.Local
        private static IEnumerable<string> GenerateFirstGreen(char characterAtTheWidestPoint)
        {
            yield return $"{InitialChar}";
        }
        
        // ReSharper disable once UnusedMember.Local
        private static IEnumerable<string> GenerateSecondGreen(char characterAtTheWidestPoint)
        {
            if (characterAtTheWidestPoint == InitialChar)
            {
                yield return $"{InitialChar}";
            }
            else
            {
                yield return $" {InitialChar} ";
                yield return $"{characterAtTheWidestPoint} {characterAtTheWidestPoint}";
                yield return $" {InitialChar} ";
            }
        }
        
        private static void AssertIsCapitalLetter(char characterAtTheWidestPoint)
        {
            if (!Regex.IsMatch($"{characterAtTheWidestPoint}", "[A-Z]"))
                throw new ArgumentOutOfRangeException(nameof(characterAtTheWidestPoint),
                    $"El caracter '{characterAtTheWidestPoint}' es inválido. Sólo se admiten letras mayúsculas.");
        }

        private static IEnumerable<string> GenerateDiamondRecursively(char currentChar, char maxChar, int lineLength)
        {
            var line = GenerateLine(currentChar, maxChar, lineLength);
            if (currentChar == maxChar) return new List<string> {line};
            return GenerateDiamondRecursively(Convert.ToChar(currentChar + 1), maxChar, lineLength)
                .Prepend(line).Append(line);
        }

        private static IEnumerable<string> GenerateDiamondIterative(char characterAtTheWidestPoint)
        {
            var totalDistance = characterAtTheWidestPoint - InitialChar;
            var totalLength = totalDistance * 2 + 1;
            for (var i = InitialChar; i < characterAtTheWidestPoint; i++)
                yield return GenerateLine(i, characterAtTheWidestPoint, totalLength);

            for (var i = characterAtTheWidestPoint; i >= InitialChar; i--)
                yield return GenerateLine(i, characterAtTheWidestPoint, totalLength);
        }

        private static string GenerateLine(char currentChar, char maxChar, int totalLength)
        {
            if (currentChar == InitialChar) return GenerateLineForInitialChar(totalLength);
            var sidePaddingPlusCurrentCharLength = maxChar - currentChar + 1;
            var middlePadding = string.Empty.PadLeft(totalLength - sidePaddingPlusCurrentCharLength * 2, Padding);
            return
                $"{currentChar.ToString().PadLeft(sidePaddingPlusCurrentCharLength, Padding)}{middlePadding}{currentChar.ToString().PadRight(sidePaddingPlusCurrentCharLength, Padding)}";
        }

        private static string GenerateLineForInitialChar(int totalLength)
        {
            var paddingChars = string.Empty.PadLeft((totalLength - 1) / 2, Padding);
            return $"{paddingChars}{InitialChar}{paddingChars}";
        }
    }    
}

